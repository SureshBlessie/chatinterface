import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import Page from '../../../components/Page';
import Results from './Results';
import Toolbar from './Toolbar';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const UserListView = () => {
  const classes = useStyles();
  const url1 = "http://127.0.0.1:2345";
  const url1ChatRooms = "http://127.0.0.1:2345/chatrooms";
  const [chats, setChats] = useState([]);
  useEffect(() => {
      const fetchData = async () => {
      const result = await axios(url1ChatRooms);
      const chatData = result.data;
      var values = [];
      for (var ln = 0; ln < chatData.length; ln++) {
          var item1 = {
            "id": ln,"server":url1,"chatroom":chatData[ln]
          };
          values.push(item1);
        }
        setChats(values);
    };
    fetchData();
  }, []);



  return (
    <Page
      className={classes.root}
      title="Users"
    >
      <Container maxWidth={false}>
        <Toolbar />
        <Box mt={3}>
          <Results customers={chats} />
        </Box>
      </Container>
    </Page>
  );
};

export default UserListView;
