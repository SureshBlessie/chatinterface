import React, { useState,useEffect } from 'react';
import clsx from 'clsx';
import axios from 'axios';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  Grid,
  Typography,
  colors,
  makeStyles
} from '@material-ui/core';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%'
  },
  avatar: {
    backgroundColor: colors.green[600],
    height: 56,
    width: 56
  },
  differenceIcon: {
    color: colors.green[900]
  },
  differenceValue: {
    color: colors.green[900],
    marginRight: theme.spacing(1)
  }
}));

const ChatRooms = ({ className, ...rest }) => {
  const classes = useStyles();
  const url1 = "http://127.0.0.1:2345";
  const url1ChatRooms = "http://127.0.0.1:2345/chatrooms";
  const [chats, setChats] = useState([]);
  useEffect(() => {
      const fetchData = async () => {
      const result = await axios(url1ChatRooms);
      const chatData = result.data;
      var values = [];
      for (var ln = 0; ln < chatData.length; ln++) {
          var item1 = {
            "id": ln,"server":url1
          };
          values.push(item1);
        }
        setChats(values);
        console.log(values);
    };
    fetchData();
  }, []);

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardContent>
        <Grid
          container
          justify="space-between"
          spacing={3}
        >
          <Grid item>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="h6"
            >
              Chat Rooms
            </Typography>
            <Typography
              color="textPrimary"
              variant="h1"
            >
              {chats.length}
            </Typography>
            <Typography
              color="textSecondary"
              variant="h6"
            >
              on servers :
              <Results customers={chats} />
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

ChatRooms.propTypes = {
  className: PropTypes.string
};

export default ChatRooms;
