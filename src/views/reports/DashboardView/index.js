import React from 'react';
import {
  Container,
  Grid,
  makeStyles
} from '@material-ui/core';

import Page from '../../../components/Page';
import ActiveUsers from './ActiveUsers';
import ChatRooms from './ChatRooms';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const Dashboard = () => {
  const classes = useStyles();

  return (
    <Page
      className={classes.root}
      title="Chat Servers Monitoring"
    >
      <Container maxWidth={false}>
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            lg={50}
            sm={100}
            xl={3}
            xs={12}
          >
            <ActiveUsers />
          </Grid>
          <Grid
            item
            lg={50}
            sm={100}
            xl={3}
            xs={12}
          >
            <ChatRooms />
          </Grid>

        </Grid>
      </Container>
    </Page>
  );
};

export default Dashboard;
