import React, { useState,useEffect } from 'react';
import axios from 'axios';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  Grid,
  Typography,
  colors,
  makeStyles
} from '@material-ui/core';

import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%'
  },
  avatar: {
    backgroundColor: colors.red[600],
    height: 56,
    width: 56
  },
  differenceIcon: {
    color: colors.red[900]
  },
  differenceValue: {
    color: colors.red[900],
    marginRight: theme.spacing(1)
  }
}));

const ActiveUsers = ({ className, ...rest }) => {
  const classes = useStyles();
  //const [servers] = useState(data);

  const url1 = "http://127.0.0.1:2345";
  const url1ChatRooms = "http://127.0.0.1:2345/messages/0";
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
    const result = await axios(url1ChatRooms);
    const chatData = result.data;
    var values = [];
    for (var ln = 0; ln < chatData.length; ln++) {
        var localData = chatData[ln];
        var item1 = {
          "id": ln,"server":url1,"name":localData.sender.account.username,"userid":localData.sender.account.id,"status":localData.sender.currentStatus
        };
        values.push(item1);
      }
      setUsers(values);
  };
  fetchData();
}, []);

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardContent>
        <Grid
          container
          justify="space-between"
          spacing={3}
        >
          <Grid item>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="h6"
            >
              Active users
            </Typography>
            <Typography
              color="textPrimary"
              variant="h1"
            >
              {users.length}
            </Typography>
            <Typography
              color="textSecondary"
              variant="h6"
            >
              on servers :
              <Results customers={users} />
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

ActiveUsers.propTypes = {
  className: PropTypes.string
};

export default ActiveUsers;
