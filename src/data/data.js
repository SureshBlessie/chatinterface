import { v4 as uuid } from 'uuid';

export default [
  {
    id: uuid(),
    userid: '0',
    avatarUrl: '/static/images/avatars/avatar_7.png',
    server: 'http://127.0.0.1:4567',
    name: 'Emilee Simchenko',
    status: 'ACTIVE',
    chatroom: 'chat1'
  }, {
    id: uuid(),
    userid: '1',
    avatarUrl: '/static/images/avatars/avatar_8.png',
    server: 'http://127.0.0.1:2435',
    name: 'Kwak Seong-Min',
    status: 'INACTIVE',
    chatroom: 'chat2'
  },
  {
    id: uuid(),
    userid: '1',
    avatarUrl: '/static/images/avatars/avatar_9.png',
    server: 'http://127.0.0.1:2435',
    name: 'Kwak Seong-Min',
    status: 'INACTIVE',
    chatroom: 'chat2'
  },
  {
    id: uuid(),
    userid: '2',
    avatarUrl: '/static/images/avatars/avatar_10.png',
    server: 'http://127.0.0.1:1234',
    name: 'Merrile Burgett',
    status: 'ACTIVE',
    chatroom: 'chat3'
  }
];
